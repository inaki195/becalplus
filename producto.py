import flask
from flask_mysqldb import MySQL
from core import mysql,app
from flask_login import login_required
from cryptography.fernet import Fernet

import os
import utils
from datetime import date
from datetime import datetime
from core import key
from flask import render_template,redirect,url_for,request
producto=flask.Blueprint('productos',__name__)

@producto.route('/productos')
def productoGeneral():
    product=utils.getAll("productos")
    return render_template("productos.html",productos=product)

@producto.route('/crear-producto', methods=['GET', 'POST'])
@login_required
def createNoticia():
     if request.method == 'POST':
            file=request.files['imagen']
            file.save(os.path.join(app.config["UPLOAD_PRODUCTS"],file.filename))
            producto = request.form['producto']
            descripcion = request.form['descripcion']
            precio= request.form['balloons']
            cursor = mysql.connection.cursor()
            img=file.filename
            cursor.execute(f"insert into productos(nombre,descripcion,precio,img) values(%s,%s,%s,%s)",
                           (producto,descripcion,precio,img))
            cursor.connection.commit()

            return redirect(url_for("productos.productoGeneral"))
     return render_template('createproductos.html')

@producto.route('/editar-producto/<producto_id>',methods=['GET', 'POST'])
@login_required
def editProducto(producto_id):
    if  request.method == 'POST':
        file=request.files['imagen']
        img=file.filename
        producto = request.form['producto']
        descripcion = request.form['descripcion']
        precio= request.form['balloons']
        cursor = mysql.connection.cursor()
        cursor.execute(f"""
        UPDATE productos
        set nombre='{producto}',
        img='{img}',
        descripcion='{descripcion}',
        precio='{precio}'
        where id={producto_id};
        """)
        cursor.connection.commit()
        return redirect(url_for("productos.productoGeneral"))
    cursor = mysql.connection.cursor()
    cursor.execute(f"select * from productos where id={producto_id}")
    producto=cursor.fetchall()
    return render_template("createproductos.html",producto=producto[0])

@producto.route('/borrar-producto/<producto_id>')
@login_required
def deleteProducto(producto_id):
    cursor = mysql.connection.cursor()
    cursor.execute(f"delete from productos where id={producto_id}")
    mysql.connection.commit()
    return redirect(url_for("productos.productoGeneral"))

@producto.route('/producto-detalle/<producto_id>')
def readProducto(producto_id):
    producto=utils.buscarProducto(producto_id)
    return render_template("detalleproducto.html",producto=producto)