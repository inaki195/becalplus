from flask import Flask, render_template,flash, request, redirect, session,  url_for, request
from datetime import datetime;
from flask_wtf.csrf import CSRFProtect
from werkzeug.utils import secure_filename
import os
import utils
import core

from plantilla_routes import plantilla
from noticias import noticia
from login import loginb
from core import app
from producto import producto
#agregando los blueprints
app.register_blueprint(producto)
app.register_blueprint(loginb)
app.register_blueprint(plantilla)
app.register_blueprint(noticia)
ALLOWED_EXTENSIONS=set(['png','jpg','jpeg','gif'])
csrf=CSRFProtect()
@app.route('/',methods=['GET', 'POST'])
def inicio():
      noticias=utils.getNews()
      if request.method == 'POST':
            nombre = request.form['nombre']
            email = request.form['email']
            msg= request.form['message']
            utils.sendEmail(nombre,email,msg,"static/credit/clave.key","static/credit/key.txt",
            "inakimusic59@gmail.com","ifernandezbescos@gmail.com")
      return render_template("index.html",noticias=noticias)

@app.route('/prueba')
def prueba():
    passwd=utils.getPassword()
    return f"{passwd}"
@app.route('/novedades')
def novedades():
    noticias=utils.getAll("noticias")
    return render_template("novedades.html",noticias=noticias)
@app.route('/quienes-somos')
def quienSomos():
    people=utils.getAll('personal')
    return render_template("quien_somos.html",people=people)

@app.route('/nuestros-producto')
def productosV():
    productos=utils.getAll('productos')
    return render_template("productosf.html",productos=productos)

@app.route('/unete-nuestro-equipo',methods=['GET', 'POST'])
def uneteEmpresa():
     if request.method == 'POST':
        nameComplete=""
        name = request.form['nombre']
        apellido=request.form['apellido']
        telefono=request.form['telefono']
        email = request.form['email']
        mensaje= request.form['mensaje']
        nombre="Hola soy "+name+" "+apellido
        msg=f"{mensaje} . \n este es mi email : {email}  y este  mi numero de telefono {telefono}"
        utils.sendEmail(nombre,email,msg,"static/credit/clave2.key","static/credit/key2.txt",
            "talento.belcalplus@gmail.com","r.esteban@becalplus.es")
             
        flash('tus datos se enviaron correctamente')
        return redirect(url_for('uneteEmpresa'))
     return render_template("unete.html")


@app.route('/soluciones')
def soluciones():
    productos=utils.getAll('productos')
    return render_template("solucion.html",productos=productos)


def pageNotFound(error):
    return render_template("error.html")


if __name__ == '__main__':
    csrf.init_app(app)
    app.register_error_handler(404,pageNotFound)
    app.run(debug=True)
