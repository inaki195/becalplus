import flask
from flask_mysqldb import MySQL
from core import mysql,app
from flask_login import login_required
import os
import utils
from datetime import date
from datetime import datetime
from flask import render_template,redirect,url_for,request
noticia=flask.Blueprint('noticias',__name__)

@noticia.route('/noticia')
def funciona():
    news=utils.getAll("noticias")
    return render_template("noticias.html",noticias=news)

@noticia.route('/crear-noticia', methods=['GET', 'POST'])
@login_required
def createNoticia():
     if request.method == 'POST':
            file=request.files['imagen']
            file.save(os.path.join(app.config["UPLOAD_NEWS"],file.filename))
            titulo = request.form['titulo']
            cuerpo = request.form['cuerpo']
            resumen= request.form['resumen']
            now = datetime.now()
            cursor = mysql.connection.cursor()
            img=file.filename
            cursor.execute(f"insert into noticias(titulo,descripcion,fecha_publicacion,img,resumen) values(%s,%s,%s,%s,%s)",
                           (titulo, cuerpo, now,img,resumen))
            cursor.connection.commit()

            return redirect(url_for("noticias.funciona"))
     return render_template('createnoticias.html')

@noticia.route('/borrar-noticia/<noticia_id>')
@login_required
def deleteNoticia(noticia_id):
    cursor = mysql.connection.cursor()
    cursor.execute(f"delete from noticias where id={noticia_id}")
    mysql.connection.commit()
    return redirect(url_for("noticias.funciona"))

@noticia.route('/noticia-detalle/<noticia_id>')
def readNoticia(noticia_id):
    noticia=utils.buscarNoticia(noticia_id)
    return render_template("detalle.html",noticia=noticia)



@noticia.route('/editar-noticia/<noticia_id>',methods=['GET', 'POST'])
@login_required
def editNoticia(noticia_id):
    if  request.method == 'POST':
        file=request.files['imagen']
        resumen=request.form['resumen']
        titulo = request.form['titulo']
        desc = request.form['cuerpo']
        img=file.filename
        cursor = mysql.connection.cursor()
        cursor.execute(f"""
        UPDATE noticias
        set img ='{img}',
        titulo='{titulo}',
        resumen='{resumen}',
         descripcion='{desc}'
        where id={noticia_id};
        """)
        cursor.connection.commit()
        return redirect(url_for("noticias.funciona"))
    cursor = mysql.connection.cursor()
    cursor.execute(f"select * from noticias where id={noticia_id}")
    noticia=cursor.fetchall()
    return render_template("createnoticias.html",noticia=noticia[0])