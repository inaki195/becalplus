import flask
from flask_mysqldb import MySQL
from core import mysql,app
import os
import utils
from flask_login import login_required
from flask import render_template,redirect,url_for,request
plantilla=flask.Blueprint('plantilla',__name__)

@plantilla.route('/borrar-personal/<persona_id>')
@login_required
def deletePersonal(persona_id):
    cursor = mysql.connection.cursor()
    cursor.execute(f"delete from personal where id={persona_id}")
    mysql.connection.commit()
    return redirect(url_for("plantilla.personal"))



@plantilla.route('/crear-personal', methods=['GET', 'POST'])
@login_required
def createPersonal():
     if request.method == 'POST':
            file=request.files['archivo']
            file.save(os.path.join(app.config["UPLOAD_FOLDER"],file.filename))
            
            nombre = request.form['nombre']
            apellido = request.form['surname']
            apellido2 = request.form['surname2']
            puesto = request.form['puesto']
            link=request.form['link']
            cursor = mysql.connection.cursor()
            img=file.filename
            cursor.execute(f"insert into personal(nombre,apellido_f,apellido_s,img,puesto,linkdin) values(%s,%s,%s,%s,%s,%s)",
                           (nombre, apellido, apellido2, img,puesto,link))
            cursor.connection.commit()

            return redirect(url_for("inicio"))
     return render_template('personalcreate.html')
@plantilla.route('/personal')
@login_required
def personal():
    people =utils.getAll("personal")
    return render_template('personal.html',people=people)

@plantilla.route('/editar-personal/<persona_id>',methods=['GET', 'POST'])
@login_required
def editPersonal(persona_id):
    if  request.method == 'POST':
        nombre = request.form['nombre']
        apellido = request.form['surname']
        apellido2 = request.form['surname2']
        puesto = request.form['puesto']
        link=request.form['link']
        img=""
        cursor = mysql.connection.cursor()
        cursor.execute(f"""
        UPDATE personal
        set nombre='{nombre}',
         apellido_f='{apellido}',
         apellido_s='{apellido2}',
         puesto='{puesto}',
         linkdin='{link}'
        where id={persona_id};

        """)
        cursor.connection.commit()
        return redirect(url_for("plantilla.personal"))
    cursor = mysql.connection.cursor()
    cursor.execute(f"select * from personal where id={persona_id}")
    persona=cursor.fetchall()
    return render_template("personalcreate.html",persona=persona[0])



