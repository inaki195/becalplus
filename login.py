from re import L
import flask
from flask import render_template,redirect,url_for,request,flash
from model import User
from flask_wtf.csrf import CSRFProtect
#modelos
from core import app
from flask_login import LoginManager,login_user,logout_user,login_required
from model import ModelUser
loginb=flask.Blueprint('loginb',__name__)
app.secret_key = 'secret'
login_manager_app=LoginManager(app)
csrf=CSRFProtect()

@login_manager_app.user_loader
def load_user(id):
       return  ModelUser.get_by_id(id)
@loginb.route('/login',methods=['GET', 'POST'])
def login():
 if request.method == 'POST':
        user=User(0,request.form['usuario'],request.form['password'])
        logged_user=ModelUser.login(user)
        if logged_user!=None:
               if logged_user.password:
                      login_user(logged_user)
                      return redirect(url_for('loginb.inicioBackend'))
               else:
                      flash("invalid password")
                      return render_template('login.html')
        else:
         flash("usuario no encotrado")
        print(request.form['usuario'])
        print(request.form['password'])
        return render_template('login.html')
 else:
        return render_template('login.html')

@loginb.route('/inicio')
@login_required
def inicioBackend():
       return render_template('init.html')

@loginb.route('/prote')
@login_required
def prueback():
       return  '<h2>vista protegida</h2>'

@loginb.route('/logout')
def logout():
       logout_user()
       return redirect(url_for('loginb.login'))
