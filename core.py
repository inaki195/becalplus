from flask import Flask
from flask_mysqldb import MySQL
from cryptography.fernet import Fernet
# conexion a base de datos
key=Fernet.generate_key()
app = Flask(__name__)
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'becalplus'
mysql=MySQL(app)
app.config['UPLOAD_FOLDER']="static/pictures/personal"
app.config['UPLOAD_NEWS']="static/pictures/news"
app.config['UPLOAD_PRODUCTS']="static/pictures/productos"
app.config['CREDENCIALES']="static/credit/key.txt"