from core import mysql
import smtplib
from cryptography.fernet import Fernet
#-*- coding: utf-8 -*-
def getNews():
    cursor = mysql.connection.cursor()
    cursor.execute(f"select * from noticias order by fecha_publicacion desc limit 4")
    people=cursor.fetchall()
    cursor.close()
    return people

def getAll(table):
    cursor = mysql.connection.cursor()
    cursor.execute(f"select  * from {table}")
    all=cursor.fetchall()
    cursor.close()
    return all
def createpaswwd():
    generar_clave("static/credit/clave2.key")
    clave2=cargar_clave("static/credit/clave2.key")
    name_file="static/credit/key2.txt"
    encriptarArchivo(name_file,clave2)

def getPassword():
     clave2=cargar_clave("static/credit/clave2.key")
     name_file="static/credit/key2.txt"
     return desencrip(name_file,clave2)

def buscarNoticia(id):
    cursor = mysql.connection.cursor()
    cursor.execute(f"select  * from noticias where id={id}")
    people=cursor.fetchone()
    cursor.close()
    return people

def buscarProducto(id):
    cursor = mysql.connection.cursor()
    cursor.execute(f"select  * from productos where id={id}")
    people=cursor.fetchone()
    cursor.close()
    return people

def sendEmail(nombre,email,msg,file_key,file,email_emisor,email_receptor):
    server=smtplib.SMTP('smtp.gmail.com',587)
    send_email=email_emisor
    rec_email=email_receptor
    contenido=f"""
    {nombre} {email} \n
    {msg}
    """
    contenido=contenido.encode(encoding = 'UTF-8')
    key=Fernet.generate_key()
    #codificando  a utf8
    
  
    clave=cargar_clave(file_key)
    name_file=file
    passwd=desencrip(name_file,clave)
    server.starttls()
    server.login(send_email,passwd)
    server.sendmail(send_email,rec_email,contenido)
    
def encriptarArchivo(name,clave):
    f=Fernet(clave)
    with open(name,"rb") as file:
        archivo_info=file.read()
    encrypted_data=f.encrypt(archivo_info)
    with open(name,"wb")as file:
        file.write(encrypted_data)

def desencrip(n_archivo,clave):
    f=Fernet(clave)
    password=""
    with open(n_archivo,"rb")as file:
        encryp_data=file.read()
    password=descrypted_data=f.decrypt(encryp_data)
    password=password.decode()
    return password
def generar_clave(path_key):
    clave=Fernet.generate_key()
    with open(path_key,"wb") as archivo:
        archivo.write(clave)

def cargar_clave(file_key):
    return open(file_key,'rb').read()


